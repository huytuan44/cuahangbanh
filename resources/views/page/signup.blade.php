@extends('master')
@section('content')
<div class="inner-header">
	<div class="container">
		<div class="pull-left">
			<h6 class="inner-title">Đăng kí</h6>
		</div>
		<div class="pull-right">
			<div class="beta-breadcrumb">
				<a href="index.html">Home</a> / <span>Đăng kí</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div class="container">
	<div id="content">
		
		<form action="{{route('signup')}}" method="post" class="beta-form-checkout">
			<div class="row">
				@if(Session::has('thanhcong'))
					<div class="alert alert-success">{{Session::get('thanhcong')}} </div>
				@endif 
				<div class="col-sm-3"></div>
				<div><input type="hidden" name="_token" value="{{csrf_token()}}"></div>
				<div class="col-sm-6">
					<h4>Đăng kí</h4>
					<div class="space20">&nbsp;</div>
					
						@if($errors->has('email'))
						<div class="alert alert-danger">{{$errors->first('email')}}</div>			
						@endif
					<div class="form-block">
						<label for="email">Email address*</label>
						<input type="text" id="email" name="email">
					</div>
						@if($errors->has('fullname'))
						<div class="alert alert-danger">{{$errors->first('fullname')}}</div>			
						@endif
					<div class="form-block">
						<label for="your_last_name">Fullname*</label>
						<input type="text" id="your_last_name" name="fullname">
						
					</div>
						@if($errors->has('address'))
						<div class="alert alert-danger">{{$errors->first('address')}}</div>			
						@endif
					<div class="form-block">
						<label for="adress">Address*</label>
						<input type="text" id="adress" name="address" >
					</div>

						@if($errors->has('phone'))
						<div class="alert alert-danger">{{$errors->first('phone')}}</div>			
						@endif
					<div class="form-block">
						<label for="phone">Phone*</label>
						<input type="text" id="phone" name="phone">

					</div>
						@if($errors->has('password'))
						<div class="alert alert-danger">{{$errors->first('password')}}</div>			
						@endif
					<div class="form-block">
						<label for="password">Password*</label>
						<input type="password" id="password" name="password">

					</div>
						@if($errors->has('re-password'))
						<div class="alert alert-danger">{{$errors->first('re-password')}}</div>			
						@endif					
					<div class="form-block">
						<label for="re-password">Re password*</label>
						<input type="password" id="re-password" name="re-password">
						
					</div>
					<div class="form-block">
						<button type="submit" class="btn btn-primary">Register</button>
					</div>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection